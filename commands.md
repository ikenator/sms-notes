# DOCKER MISC

## Delete all containers
docker rm $(docker ps -a -q)

## Delete all images
docker rmi $(docker images -q)

## Enter docker container
docker exec -it smartmetserver bin/bash

## Logs

docker logs -f smartmetserver

## All the rest

sudo systemctl daemon-reload

sudo systemctl restart docker

docker images -f dangling=true

# PULL

## Backend
docker pull fmidev/smartmetserver-backend

## Frontend
docker pull fmidev/smartmetserver-frontend

# RUN

## Backend
docker run --restart=always --name smartmetserver -v $HOME/docker-smartmetserver/smartmet/data:/smartmet/data -v $HOME/docker-smartmetserver/smartmet/share:/smartmet/share/ -v $HOME/docker-smartmetserver/smartmetconf:/etc/smartmet/conf/ -p 8080:8080 fmidev/smartmetserver-backend

## Frontend
docker run --restart=always --name smartmetserver -v $HOME/docker-smartmetserver/smartmet/data:/smartmet/data -v $HOME/docker-smartmetserver/smartmet/share:/smartmet/share/ -v $HOME/docker-smartmetserver/brainstorm/cache/frontend-compressed-cache:/brainstorm/cache/frontend-compressed-cache -v $HOME/docker-smartmetserver/smartmetconf:/etc/smartmet/conf/ -p 8080:8080 fmidev/smartmetserver-frontend

# HELP

https://docs.docker.com/compose/reference/up/

# SWARM

docker node ls
docker node inspect self

## Starting

docker swarm init
docker stack deploy --compose-file=docker-compose.yml smartmettitesti

## Joining

docker swarm join --token SWMTKN-1-3pu6hszjas19xyp7ghgosyx9k8atbfcr8p2is99znpy26u2lkl-7p73s1dx5in4tatdymyhg9hu2 192.168.99.121:2377

